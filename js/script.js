"use strict";

//1. Метод forEach() выполняет указанную функцию один раз для каждого элемента в массиве.
//2. Очистить массив можно следующими способами: Array.length =0; Array.splice(0, Array.length); Array = []
//3.Проверить является ли переменная массивом можно с помощью метода Array.isArray()

const filtered = function filterBy(array, type) {
  return array.filter((element) => typeof element !== type);
};

console.log(
  filtered(
    [
      true,
      false,
      true,
      "string",
      26,
      NaN,
      78,
      undefined,
      null,
      { name: "John" },
      [1, 2, 3],
    ],
    "object"
  )
);
